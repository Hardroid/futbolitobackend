package cl.hardroid.futbolito.aspect

import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.After
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import java.util.*

@Aspect
@Configuration
class LoggingAspect {

    private val logger = LoggerFactory.getLogger(this.javaClass)


    @Before("execution( * cl.hardroid.futbolito.*.*.* (..))")
    fun before(joinPoint: JoinPoint) {
        logger.info("Check method init " + joinPoint.signature.name)
        logger.info("Method params {}", joinPoint)
    }

    @AfterReturning("execution( * cl.hardroid.futbolito.*.*.* (..))",
            returning = "result")
    fun afterReturning(joinPoint: JoinPoint, result: Object) {
        logger.info("{} returned with value {}", joinPoint, result)
    }

    @After(value = "execution(* com.in28minutes.springboot.tutorial.basics.example.aop.business.*.*(..))")
    fun after(joinPoint: JoinPoint) {
        logger.info("after execution of {}", joinPoint)
    }
}