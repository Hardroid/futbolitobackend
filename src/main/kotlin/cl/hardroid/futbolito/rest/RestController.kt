package cl.hardroid.futbolito.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import cl.hardroid.futbolito.entity.Usuario
import cl.hardroid.futbolito.repository.UsuarioRepository
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PostMapping


@RestController
@RequestMapping("futbolito")
internal class RestController {

    @Autowired
    lateinit var usuarioRepository: UsuarioRepository

    @Autowired
    lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder


    @GetMapping("/login")
    fun validarUsuario(): String {
        return "Hola Mundo"
    }

    @PostMapping("/registro")
    fun signUp(@RequestBody user: Usuario): Usuario {
        val usuario = usuarioRepository.save(user)
        return usuario
    }
}