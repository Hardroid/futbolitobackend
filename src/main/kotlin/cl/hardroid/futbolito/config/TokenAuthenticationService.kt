package cl.hardroid.futbolito.config

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import io.jsonwebtoken.Jwts
import java.util.*
import javax.servlet.http.HttpServletResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.User
import javax.servlet.http.HttpServletRequest


object TokenAuthenticationService {
    val EXPIRATIONTIME: Long = 864000000 // 10 days
    val SECRET = "ThisIsASecret"
    val TOKEN_PREFIX = "Bearer"
    val HEADER_STRING = "Authorization"

    fun addToken(response: HttpServletResponse, authResult: Authentication){
        val token = JWT.create()
                .withSubject((authResult.getPrincipal() as User).getUsername())
                .withClaim("idUsuario", "test")
                .withExpiresAt(Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .sign(Algorithm.HMAC512(SECRET.toByteArray()))
        response.addHeader(HEADER_STRING, "$TOKEN_PREFIX $token")
    }

    fun getAuthentication(request: HttpServletRequest): Authentication? {
        val token = request.getHeader(HEADER_STRING)
        if (token != null) {
            // parse the token.
            val user = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token!!.replace(TOKEN_PREFIX, ""))
                    .body
                    .subject

            return if (user != null)
                UsernamePasswordAuthenticationToken(user, null, emptyList())
            else
                null
        }
        return null
    }

}