package cl.hardroid.futbolito.config

import cl.hardroid.futbolito.config.TokenAuthenticationService.HEADER_STRING
import cl.hardroid.futbolito.config.TokenAuthenticationService.TOKEN_PREFIX
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.security.core.context.SecurityContextHolder


class JWTAuthorizationFilter(authManager : AuthenticationManager) : BasicAuthenticationFilter(authManager) {

    override fun doFilterInternal(
            request: HttpServletRequest,
            response: HttpServletResponse,
            chain: FilterChain) {
        val header = request.getHeader(HEADER_STRING)

        if (header == null || !header!!.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response)
            return
        }

        val authentication = TokenAuthenticationService.getAuthentication(request)

        SecurityContextHolder.getContext().authentication = authentication
        chain.doFilter(request, response)
    }
}