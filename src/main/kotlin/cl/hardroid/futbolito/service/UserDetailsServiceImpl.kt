package cl.hardroid.futbolito.service

import cl.hardroid.futbolito.repository.UsuarioRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UsernameNotFoundException


@Service
class UserDetailsServiceImpl : UserDetailsService {

    @Autowired lateinit var applicationUserRepository: UsuarioRepository

    override fun loadUserByUsername(username: String): UserDetails {
        val applicationUser = applicationUserRepository.findByUsername(username)
        if (applicationUser == null) {
            throw UsernameNotFoundException(username)
        }
        return User(applicationUser.username, applicationUser.password, emptyList())
    }
}