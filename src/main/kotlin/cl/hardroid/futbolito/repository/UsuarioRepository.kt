package cl.hardroid.futbolito.repository

import cl.hardroid.futbolito.entity.Usuario
import org.springframework.data.jpa.repository.JpaRepository

interface UsuarioRepository : JpaRepository<Usuario, Long>{
    fun findByUsername(username: String): Usuario
}