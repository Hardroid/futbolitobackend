package cl.hardroid.futbolito.entity

import javax.persistence.*

@Entity
data class Usuario(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val idUsuario: Long,
        @Column(unique = true, name = "username")
        val username: String,
        val posicion: String,
        val urlPerfil: String,
        val hinchaDe: String,
        val password: String
) {
        constructor() : this(0,"","","","","")
}