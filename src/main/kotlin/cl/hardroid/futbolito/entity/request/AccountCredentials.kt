package cl.hardroid.futbolito.entity.request

data class AccountCredentials(val username: String, val password: String){
    constructor() : this("", "")
}